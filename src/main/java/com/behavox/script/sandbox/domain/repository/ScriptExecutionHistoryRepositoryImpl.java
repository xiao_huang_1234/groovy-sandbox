package com.behavox.script.sandbox.domain.repository;

import com.behavox.script.sandbox.adapter.entity.ScriptExecutionHistory;
import com.behavox.script.sandbox.adapter.repository.ScriptExecutionHistoryJpaRepository;
import com.behavox.script.sandbox.config.ScriptExecutionProperties;
import com.behavox.script.sandbox.domain.exception.DefaultServerException;
import com.behavox.script.sandbox.domain.exception.ScriptExecutionResultNotFoundException;
import com.behavox.script.sandbox.domain.model.ScriptExecutionResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Repository implementation to save the script execution history
 */
@Repository
@Slf4j
public class ScriptExecutionHistoryRepositoryImpl implements ScriptExecutionHistoryRepository {

	private ScriptExecutionHistoryJpaRepository scriptExecutionHistoryRepository;

	private int maxExecutionResultCharacters;

	@Autowired
	public ScriptExecutionHistoryRepositoryImpl(ScriptExecutionHistoryJpaRepository scriptExecutionHistoryRepository,
												ScriptExecutionProperties scriptExecutionProperties) {
		this.scriptExecutionHistoryRepository = scriptExecutionHistoryRepository;
		this.maxExecutionResultCharacters = scriptExecutionProperties.getMaxResultCharacters();
	}

	/**
	 * Save the script execution history to a data store
	 *
	 * @param scriptExecutionResult		the script execution result to be saved
	 * @return							the updated script execution result
	 */
	@Override
	public ScriptExecutionResult save(ScriptExecutionResult scriptExecutionResult) {
		log.debug("Saving script execution result: {}", scriptExecutionResult);
		ScriptExecutionHistory scriptExecutionHistory = convertFrom(scriptExecutionResult);
		try {
			scriptExecutionHistory = scriptExecutionHistoryRepository.save(scriptExecutionHistory);
		} catch (Exception ex) {
			throw new DefaultServerException(
					String.format("Failed to save the script execution history: %s", scriptExecutionHistory), ex);
		}

		log.debug("Saved script execution result: {}", scriptExecutionResult);
		return convertFrom(scriptExecutionHistory);
	}

	/**
	 * Find script execution result by ID
	 *
	 * @param executionId			the script execution result ID
	 * @return						the script execution result
	 */
	@Override
	public ScriptExecutionResult findById(String executionId) {
		ScriptExecutionHistory entity = scriptExecutionHistoryRepository.findById(executionId)
				.orElseThrow(() -> new ScriptExecutionResultNotFoundException(
						String.format("Script execution result %s not found", executionId)));
		return convertFrom(entity);
	}

	/**
	 * Transform the script execution history entity to the domain model of script execution result
	 *
	 * @param entity				the script execution history entity
	 * @return						the domain model of script execution result
	 */
	// FIXME: could also be implemented as a public non-static converter method within the entity class
	private static ScriptExecutionResult convertFrom(ScriptExecutionHistory entity) {
		if (entity == null) {
			return null;
		}
		return new ScriptExecutionResult(entity.getId(), entity.getFileName(), entity.getExecutionStatus(),
				entity.getExecutionResult(), entity.getExecutionDate());
	}

	/**
	 * Transform the domain model of script execution result to the script execution history entity
	 *
	 * @param domainModel			the domain model of script execution result
	 * @return						the entity of script execution history
	 */
	private ScriptExecutionHistory convertFrom(ScriptExecutionResult domainModel) {
		if (domainModel == null) {
			return null;
		}
		return new ScriptExecutionHistory(domainModel.getExecutionId(), domainModel.getFileName(), domainModel.getExecutionStatus(),
				formatExecutionResult(domainModel.getExecutionResult()), domainModel.getExecutionDate());
	}

	/**
	 * Format the script execution result object to the formatted script execution result string
	 *
	 * @param executionResult			the script execution result object
	 * @return							the formatted script execution result string
	 */
	private String formatExecutionResult(Object executionResult) {
		if (executionResult == null) {
			return "null";
		} else {
			String result = executionResult.toString();
			if (result.length() <= maxExecutionResultCharacters) {
				return result;
			} else {
				return result.substring(0, maxExecutionResultCharacters);
			}
		}
	}

}
