package com.behavox.script.sandbox.domain.repository;

import com.behavox.script.sandbox.domain.model.ScriptExecutionResult;

/**
 * Repository that defines the service to save the script execution history
 */
public interface ScriptExecutionHistoryRepository {
	/**
	 * Save the script execution history to a data store
	 *
	 * @param scriptExecutionResult		the script execution result to be saved
	 * @return							the updated script execution result
	 */
	public ScriptExecutionResult save(ScriptExecutionResult scriptExecutionResult);

	/**
	 * Find script execution result by ID
	 *
	 * @param id			the script execution result ID
	 * @return				the script execution result
	 */
	public ScriptExecutionResult findById(String id);

}
