package com.behavox.script.sandbox.domain.model;

/**
 * Domain model to track status transition during the script execution
 */
public enum ScriptExecutionStatus {
	UPLOADED, // the script is uploaded successfully
	VALIDATED, // the script is validated successfully against the defined security rules
	VIOLATED, // the script failed to validate against the defined security rules
	SUCCESS, // the script is executed successfully
	FAILURE, // the script failed to execute
}
