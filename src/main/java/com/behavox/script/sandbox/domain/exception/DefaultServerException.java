package com.behavox.script.sandbox.domain.exception;

/**
 * Default server exception
 */
public class DefaultServerException extends RuntimeException {
    public DefaultServerException(String message) {
        super(message);
    }

    public DefaultServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
