package com.behavox.script.sandbox.domain.service;

import com.behavox.script.sandbox.domain.model.ScriptExecutionResult;
import com.behavox.script.sandbox.domain.model.ScriptExecutionStatus;
import com.behavox.script.sandbox.domain.repository.ScriptExecutionHistoryRepository;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.SecureASTCustomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

/**
 * Implementation of {@link ScriptExecutionService} service that executes Groove scripts in sandbox
 */
@Service
@Slf4j
public class GrooveExecutionServiceImpl implements ScriptExecutionService {
	private final GroovyClassLoader groovyClassLoader;
	private final GroovyShell groovyShell;

	private ScriptExecutionHistoryRepository scriptExecutionHistoryRepository;

	@Autowired
	public GrooveExecutionServiceImpl(ScriptExecutionHistoryRepository scriptExecutionHistoryRepository) {
		this.groovyClassLoader = new GroovyClassLoader(this.getClass().getClassLoader());
		CompilerConfiguration conf = new CompilerConfiguration();
		SecureASTCustomizer customizer = new SecureASTCustomizer();
		customizer.setReceiversBlackList(Arrays.asList(System.class.getName()));
		conf.addCompilationCustomizers(customizer);
		this.groovyShell = new GroovyShell(conf);
//		this.groovyShell = new GroovyShell(groovyClassLoader, new Binding());
		this.scriptExecutionHistoryRepository = scriptExecutionHistoryRepository;
	}

	/**
	 * Execute the Groove script
	 *
	 * @param scriptFile			the Groove script file to be executed
	 * @param scriptExecutionId		the script's previous execution ID
	 * @return						the script execution result
	 */
	@Override
	public ScriptExecutionResult executeScript(String scriptFile, String scriptExecutionId) {
		// TODO: validate scripts against the defined security rules
		return executeScriptWithGroovyShell(scriptFile, scriptExecutionId);
	}

	/**
	 * Execute the Groove script within Groovy shell
	 *
	 * @param scriptPath			the Groove script file to be executed
	 * @param scriptExecutionId		the script's previous execution ID
	 * @return						the script execution result
	 */
	private ScriptExecutionResult executeScriptWithGroovyShell(String scriptPath, String scriptExecutionId) {
//		CompilerConfiguration conf = new CompilerConfiguration();
//		SecureASTCustomizer customizer = new SecureASTCustomizer();
//		customizer.setReceiversBlackList(Arrays.asList(System.class.getName()));
//		conf.addCompilationCustomizers(customizer);
//		GroovyShell shell = new GroovyShell(conf);
//		Object v = shell.evaluate("System.exit(-1)");
//		System.out.println("Result = " +v);

		log.debug("Executing script {}", scriptPath);
		ScriptExecutionResult scriptExecutionResult = new ScriptExecutionResult();
		scriptExecutionResult.setExecutionId(scriptExecutionId);
		scriptExecutionResult.setFileName(scriptPath);
		try {
			File scriptFile = new File(scriptPath);
			Script script = groovyShell.parse(scriptFile);
			// TODO: override Script.run() to implement our business logic
			scriptExecutionResult.setExecutionResult(script.run());
			scriptExecutionResult.setExecutionStatus(ScriptExecutionStatus.SUCCESS);
		} catch (CompilationFailedException cfe) {
			scriptExecutionResult.setExecutionResult(cfe.getMessage());
			scriptExecutionResult.setExecutionStatus(ScriptExecutionStatus.VIOLATED);
			log.warn("Failed to parse the script {}: ", scriptPath, cfe);
		} catch (IOException ioe) {
			scriptExecutionResult.setExecutionResult(ioe.getMessage());
			scriptExecutionResult.setExecutionStatus(ScriptExecutionStatus.FAILURE);
			log.warn("Failed to execute the script {}: ", scriptPath, ioe);
		} catch (Exception ex) {
			scriptExecutionResult.setExecutionResult(ex.getMessage());
			scriptExecutionResult.setExecutionStatus(ScriptExecutionStatus.FAILURE);
			log.warn("Failed to execute the script {}: ", scriptPath, ex);
		}
		scriptExecutionResult.setExecutionDate(new Date());
		log.debug("Script {} execution result: {}", scriptPath, scriptExecutionResult);
		scriptExecutionResult = scriptExecutionHistoryRepository.save(scriptExecutionResult);
		return scriptExecutionResult;
	}

}
