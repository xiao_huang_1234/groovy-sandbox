package com.behavox.script.sandbox.domain.service;

import com.behavox.script.sandbox.config.FileUploadProperties;
import com.behavox.script.sandbox.domain.exception.FileUploadException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Default implementation of {@link FileUploadService} service that upload files to file system
 */
@Service(value = "defaultFileUploadServiceImpl")
@Slf4j
public class DefaultFileUploadServiceImpl implements FileUploadService {
    private final Path fileStorageLocation;

    @Autowired
    public DefaultFileUploadServiceImpl(FileUploadProperties fileUploadProperties) {
        this.fileStorageLocation = Paths.get(fileUploadProperties.getUploadPath()).toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileUploadException(String.format("Failed to create the upload folder %s", fileStorageLocation), ex);
        }
    }

    /**
     * Store the file to the upload path
     *
     * @param fileToUpload              the file to be uploaded
     * @return                          the path to the uploaded file
     */
    @Override
    public String uploadFile(MultipartFile fileToUpload) {
        String fileName = StringUtils.cleanPath(fileToUpload.getOriginalFilename());
        log.debug("Uploading file with original name: {}", fileName);

        try {
            // TODO: prevent overwrite by prepending target file name with UUID or timestamp
            Path targetScriptFileLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(fileToUpload.getInputStream(), targetScriptFileLocation, StandardCopyOption.REPLACE_EXISTING);
            log.debug("Uploaded file with target name: {}", targetScriptFileLocation);
            return targetScriptFileLocation.toString();
        } catch (IOException ex) {
            throw new FileUploadException(String.format("Failed to upload file %s.", fileName), ex);
        }
    }

}
