package com.behavox.script.sandbox.domain.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Interface that defines the service to upload files to various data stores
 */
public interface FileUploadService {
	/**
	 * Upload the script file to the upload data store
	 *
	 * @param fileToUpload        		the file to be uploaded
	 * @return                          the path to the uploaded file
	 */
	public String uploadFile(MultipartFile fileToUpload);
}
