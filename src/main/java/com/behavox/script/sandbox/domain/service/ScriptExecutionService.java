package com.behavox.script.sandbox.domain.service;

import com.behavox.script.sandbox.domain.model.ScriptExecutionResult;

/**
 * Interface that defines the services to execute various scripts within sandbox
 */
public interface ScriptExecutionService {
	/**
	 * Execute the script
	 *
	 * @param scriptFile			the script file to be executed
	 * @param scriptExecutionId		the script's previous execution ID
	 * @return						the script execution result
	 */
	public ScriptExecutionResult executeScript(String scriptFile, String scriptExecutionId);
}
