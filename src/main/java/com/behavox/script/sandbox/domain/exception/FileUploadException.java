package com.behavox.script.sandbox.domain.exception;

/**
 * Exception related to file upload
 */
public class FileUploadException extends RuntimeException {
    public FileUploadException(String message) {
        super(message);
    }

    public FileUploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
