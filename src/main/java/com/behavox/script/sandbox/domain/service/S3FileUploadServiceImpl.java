package com.behavox.script.sandbox.domain.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Implementation of {@link FileUploadService} service that upload files to S3
 */
@Service
@Slf4j
public class S3FileUploadServiceImpl implements FileUploadService {

    /**
     * Store the file to the upload S3 bucket
     *
     * @param fileToUpload              the file to be uploaded
     * @return                          the S3 URL for the uploaded file
     */
    @Override
    public String uploadFile(MultipartFile fileToUpload) {
        throw new UnsupportedOperationException();
    }

}
