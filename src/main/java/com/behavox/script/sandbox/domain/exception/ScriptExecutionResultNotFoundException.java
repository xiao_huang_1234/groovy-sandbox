package com.behavox.script.sandbox.domain.exception;

/**
 * Exception when script execution result cannot be found
 */
public class ScriptExecutionResultNotFoundException extends RuntimeException {
    public ScriptExecutionResultNotFoundException(String message) {
        super(message);
    }

    public ScriptExecutionResultNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
