package com.behavox.script.sandbox.domain.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Domain model for script execution result
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ScriptExecutionResult {
    private String executionId;

    private String fileName;

    private ScriptExecutionStatus executionStatus;

    private Object executionResult;

    private Date executionDate;

}
