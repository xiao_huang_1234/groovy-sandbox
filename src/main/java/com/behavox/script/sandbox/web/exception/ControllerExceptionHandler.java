package com.behavox.script.sandbox.web.exception;

import com.behavox.script.sandbox.domain.exception.DefaultServerException;
import com.behavox.script.sandbox.domain.exception.FileUploadException;
import com.behavox.script.sandbox.domain.exception.ScriptExecutionResultNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class ControllerExceptionHandler {
	@ResponseStatus(HttpStatus.NOT_FOUND) // 404
	@ExceptionHandler(ScriptExecutionResultNotFoundException.class)
	public void handleNotFound(ScriptExecutionResultNotFoundException ex) {
		log.error("Script execution result not found");
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
	@ExceptionHandler(FileUploadException.class)
	public void handleFileUploadException(FileUploadException ex) {
		log.error("An error occurred processing file upload" + ex);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
	@ExceptionHandler(DefaultServerException.class)
	public void handleDefaultServerException(DefaultServerException ex) {
		log.error("An error occurred processing request" + ex);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
	@ExceptionHandler(Exception.class)
	public void handleGeneralError(Exception ex) {
		log.error("An error occurred processing request" + ex);
	}
}
