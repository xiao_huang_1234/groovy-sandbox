package com.behavox.script.sandbox.web.dto;


import com.behavox.script.sandbox.domain.model.ScriptExecutionResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Response DTO for Script Execution Result
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ScriptExecutionResponse {
    private String executionId;
    private String executionStatus;
    private Object executionResult;

    public static ScriptExecutionResponse from(ScriptExecutionResult scriptExecutionResult) {
        if (scriptExecutionResult == null) {
            return null;
        }
        return new ScriptExecutionResponse(scriptExecutionResult.getExecutionId(),
                scriptExecutionResult.getExecutionStatus().name(),
                scriptExecutionResult.getExecutionResult());
    }
}
