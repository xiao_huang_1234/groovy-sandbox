package com.behavox.script.sandbox.web.controller;

import com.behavox.script.sandbox.domain.model.ScriptExecutionResult;
import com.behavox.script.sandbox.domain.model.ScriptExecutionStatus;
import com.behavox.script.sandbox.domain.repository.ScriptExecutionHistoryRepository;
import com.behavox.script.sandbox.domain.service.FileUploadService;
import com.behavox.script.sandbox.domain.service.ScriptExecutionService;
import com.behavox.script.sandbox.web.dto.ScriptExecutionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * Controller that provides sandbox to run scripts
 * <p>
 *     For scalability improvement, the following functionalities are provided:
 *     #1. The following endpoint @PostMapping("/upload") just uploads the script;
 *     #2. and save the execution history to DB without execution;
 *     #3. TODO: then fire a message to a message broker to notify background workers;
 *     #4. immediately return the execution history ID to the frontend client to query later;
 *
 *     #5. TODO: Background workers (Saga pattern could be applied here) pick up a message from the message broker,
 *     then execute the uploaded script asynchronously(simulated by the following endpoint @PostMapping("/execution/{scriptExecutionId}"));
 *
 *     #6. The following endpoint @GetMapping("/execution/{scriptExecutionId}") could be used by frontend client to later query the execution result;
 * </p>
 */
@RestController
@RequestMapping({ "/v1/scripts" })
@Slf4j
// TODO: Implement security policies for authentication, authorization and accounting using OAuth2?
public class ScriptSandboxController {
    private FileUploadService fileUploadService;
    private ScriptExecutionService scriptExecutionService;
    private ScriptExecutionHistoryRepository scriptExecutionHistoryRepository;

    @Autowired
    public ScriptSandboxController(@Qualifier(value = "defaultFileUploadServiceImpl") FileUploadService fileUploadService,
                                   ScriptExecutionService scriptExecutionService,
                                   ScriptExecutionHistoryRepository scriptExecutionHistoryRepository) {
        this.fileUploadService = fileUploadService;
        this.scriptExecutionService = scriptExecutionService;
        this.scriptExecutionHistoryRepository = scriptExecutionHistoryRepository;
    }

    /**
     * Execute the uploaded script
     * @param scriptFileToUpload        the script file to upload
     * @return                          the script execution result
     */
    @PostMapping("/upload_execute")
    public ScriptExecutionResponse uploadAndExecuteScriptFile(@RequestParam("file") MultipartFile scriptFileToUpload) {
        String uploadedScriptFile = fileUploadService.uploadFile(scriptFileToUpload);

        ScriptExecutionResult scriptExecutionResult = scriptExecutionService.executeScript(uploadedScriptFile, null);
        return ScriptExecutionResponse.from(scriptExecutionResult);
    }

    /**
     * Upload the script only, the script will be later executed by background workers
     *
     * @param scriptFileToUpload        the script file to upload
     * @return                          the script execution result
     */
    @PostMapping("/upload")
    public ScriptExecutionResponse uploadScriptFile(@RequestParam("file") MultipartFile scriptFileToUpload) {
        String uploadedScriptFile = fileUploadService.uploadFile(scriptFileToUpload);

        ScriptExecutionResult scriptExecutionResult = new ScriptExecutionResult();
        scriptExecutionResult.setFileName(uploadedScriptFile);
        scriptExecutionResult.setExecutionStatus(ScriptExecutionStatus.UPLOADED);
        scriptExecutionResult.setExecutionDate(new Date());

        scriptExecutionResult = scriptExecutionHistoryRepository.save(scriptExecutionResult);
        // TODO: scalability improvement functionality #3. then fire a message to a message broker to notify background workers;
        return ScriptExecutionResponse.from(scriptExecutionResult);
    }

    /**
     * Execute the uploaded script by execution ID
     *
     * @param scriptExecutionId                 the script execution ID
     * @return                                  the script execution result
     */
    // The scalability improvement functionality #5 is simulated here
    @PostMapping("/execution/{scriptExecutionId}")
    public ScriptExecutionResponse executeScriptById(@PathVariable String scriptExecutionId) {
        String uploadedScriptFile = scriptExecutionHistoryRepository.findById(scriptExecutionId).getFileName();
        ScriptExecutionResult scriptExecutionResult = scriptExecutionService.executeScript(uploadedScriptFile, scriptExecutionId);
        return ScriptExecutionResponse.from(scriptExecutionResult);
    }

    /**
     * Get the script execution result by execution ID
     *
     * @param scriptExecutionId                 the script execution ID
     * @return                                  the script execution result
     */
    // The scalability improvement functionality #6 is implemented here
    @GetMapping("/execution/{scriptExecutionId}")
    public ScriptExecutionResponse getExecutionHistoryById(@PathVariable String scriptExecutionId) {
        ScriptExecutionResult scriptExecutionResult =  scriptExecutionHistoryRepository.findById(scriptExecutionId);
        return ScriptExecutionResponse.from(scriptExecutionResult);
    }

}
