package com.behavox.script.sandbox;

import com.behavox.script.sandbox.config.FileUploadProperties;
import com.behavox.script.sandbox.config.ScriptExecutionProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
		FileUploadProperties.class,
		ScriptExecutionProperties.class
})
public class ScriptExecutionSandboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScriptExecutionSandboxApplication.class, args);
	}
}
