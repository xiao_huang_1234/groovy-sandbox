package com.behavox.script.sandbox.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration for script execution
 */
@ConfigurationProperties(prefix = "execution")
public class ScriptExecutionProperties {
    private int maxResultCharacters;

    public int getMaxResultCharacters() {
        return maxResultCharacters;
    }

    public void setMaxResultCharacters(int maxResultCharacters) {
        this.maxResultCharacters = maxResultCharacters;
    }

}
