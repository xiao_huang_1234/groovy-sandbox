package com.behavox.script.sandbox.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration for file upload
 */
@ConfigurationProperties(prefix = "file")
public class FileUploadProperties {
    private String uploadPath;

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }
}
