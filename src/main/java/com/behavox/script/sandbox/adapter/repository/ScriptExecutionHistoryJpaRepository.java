package com.behavox.script.sandbox.adapter.repository;

import com.behavox.script.sandbox.adapter.entity.ScriptExecutionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for {@link ScriptExecutionHistory}
 */
@Repository
public interface ScriptExecutionHistoryJpaRepository extends JpaRepository<ScriptExecutionHistory, String> {
}
