package com.behavox.script.sandbox.adapter.entity;

import com.behavox.script.sandbox.domain.model.ScriptExecutionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Entity class for script execution history
 */
@Entity
@Table(name = "script_execution_history")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ScriptExecutionHistory {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    // TODO: Event Sourcing pattern can be applied here to keep track of how the script was executed step by step
    // private String previousExecutionId;

    private String fileName;

    @Enumerated(EnumType.STRING)
    private ScriptExecutionStatus executionStatus;

    private String executionResult;

    private Date executionDate;

    public ScriptExecutionHistory(String fileName, ScriptExecutionStatus executionStatus, String executionResult) {
        this.fileName = fileName;
        this.executionStatus = executionStatus;
        this.executionResult = executionResult;
        this.executionDate = new Date();
    }

}
