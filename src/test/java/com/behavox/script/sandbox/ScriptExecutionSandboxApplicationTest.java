package com.behavox.script.sandbox;

import com.behavox.script.sandbox.web.dto.ScriptExecutionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.io.File;
import java.nio.file.Files;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ScriptExecutionSandboxApplicationTest {
    // path to test scripts
    private static final String TEST_RESOURCES_PATH = "src/test/resources/";
    // settings for "Upload and Execute Scripts" APIs
    private static final String API_SCRIPTS_EXECUTION_URL = "/v1/scripts/execution/";
    private static final String API_SCRIPTS_UPLOAD_URL = "/v1/scripts/upload";
    private static final String API_SCRIPTS_UPLOAD_AND_EXECUTE_URL = "/v1/scripts/upload_execute";
    private static final String API_SCRIPTS_UPLOAD_REQUEST_PARAMETER_MULTIPART_FILE = "file";
    private static final String API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID = "executionId";
    private static final String API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS = "executionStatus";
    private static final String API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT = "executionResult";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * A suit of test cases that demonstrate the asynchronous process that upload the script, then execute the script later,
     * finally query the script execution result.
     */
    @Test
    public void testUploadGroovyScriptThenExecuteLaterSuccessfully() throws Exception {
        final String fileName = "function_return_value_success.groovy";
        File file = new File(TEST_RESOURCES_PATH + fileName);
        MockMultipartFile upload = new MockMultipartFile(API_SCRIPTS_UPLOAD_REQUEST_PARAMETER_MULTIPART_FILE, fileName,
                MediaType.APPLICATION_OCTET_STREAM_VALUE,
                Files.readAllBytes(file.toPath()));
        // only upload the script, will execute it asynchronously
        ResultActions resultActions = mockMvc.perform(multipart(API_SCRIPTS_UPLOAD_URL).file(upload))
                .andExpect(status().isOk());

        // parse the upload script endpoint response
        MvcResult mvcResult = resultActions.andReturn();
        String responseContentString = mvcResult.getResponse().getContentAsString();
        ScriptExecutionResponse scriptExecutionResponse = objectMapper.readValue(responseContentString, ScriptExecutionResponse.class);
        final String scriptExecutionId = scriptExecutionResponse.getExecutionId();

        // query the script execution result after it is uploaded
        mockMvc.perform(get(API_SCRIPTS_EXECUTION_URL + scriptExecutionId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, is(scriptExecutionId)))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("UPLOADED")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, is("null")));

        // asynchronously execute the script after it is uploaded
        mockMvc.perform(post(API_SCRIPTS_EXECUTION_URL + scriptExecutionId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, is(scriptExecutionId)))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("SUCCESS")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, is("6")));

        // query the script execution result after it is uploaded and then executed
        mockMvc.perform(get(API_SCRIPTS_EXECUTION_URL + scriptExecutionId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, is(scriptExecutionId)))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("SUCCESS")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, is("6")));
    }

    // Below are various test cases that upload and execute the script synchronously
    @Test
    public void testUploadAndExecuteGroovyScriptSuccessfullyWhenFunctionReturnValue() throws Exception {
        final String fileName = "function_return_value_success.groovy";
        File file = new File(TEST_RESOURCES_PATH + fileName);
        MockMultipartFile upload = new MockMultipartFile(API_SCRIPTS_UPLOAD_REQUEST_PARAMETER_MULTIPART_FILE, fileName,
                MediaType.APPLICATION_OCTET_STREAM_VALUE,
                Files.readAllBytes(file.toPath()));
        mockMvc.perform(multipart(API_SCRIPTS_UPLOAD_AND_EXECUTE_URL).file(upload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, notNullValue()))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("SUCCESS")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, is("6")));
    }

    @Test
    public void testUploadAndExecuteGroovyScriptFailedWhenFunctionDoesNotExist() throws Exception {
        final String fileName = "function_not_exist_failure.groovy";
        File file = new File(TEST_RESOURCES_PATH + fileName);
        MockMultipartFile upload = new MockMultipartFile(API_SCRIPTS_UPLOAD_REQUEST_PARAMETER_MULTIPART_FILE, fileName,
                MediaType.APPLICATION_OCTET_STREAM_VALUE,
                Files.readAllBytes(file.toPath()));
        mockMvc.perform(multipart(API_SCRIPTS_UPLOAD_AND_EXECUTE_URL).file(upload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, notNullValue()))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("FAILURE")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, containsStringIgnoringCase("No signature of method")));
    }

    @Test
    public void testUploadAndExecuteGroovyScriptSuccessfullyWhenClassReturnVoid() throws Exception {
        final String fileName = "class_return_void_success.groovy";
        File file = new File(TEST_RESOURCES_PATH + fileName);
        MockMultipartFile upload = new MockMultipartFile(API_SCRIPTS_UPLOAD_REQUEST_PARAMETER_MULTIPART_FILE, fileName,
                MediaType.APPLICATION_OCTET_STREAM_VALUE,
                Files.readAllBytes(file.toPath()));
        mockMvc.perform(multipart(API_SCRIPTS_UPLOAD_AND_EXECUTE_URL).file(upload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, notNullValue()))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("SUCCESS")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, is("null")));
    }

    @Test
    public void testUploadAndExecuteGroovyScriptSuccessfullyWhenClassReturnObject() throws Exception {
        final String fileName = "class_return_object_success.groovy";
        File file = new File(TEST_RESOURCES_PATH + fileName);
        MockMultipartFile upload = new MockMultipartFile(API_SCRIPTS_UPLOAD_REQUEST_PARAMETER_MULTIPART_FILE, fileName,
                MediaType.APPLICATION_OCTET_STREAM_VALUE,
                Files.readAllBytes(file.toPath()));
        mockMvc.perform(multipart(API_SCRIPTS_UPLOAD_AND_EXECUTE_URL).file(upload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, notNullValue()))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("SUCCESS")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, containsStringIgnoringCase("Marshall")));
    }

    // Below are test cases that only uploads the script
    @Test
    public void testUploadGroovyScriptSuccessfully() throws Exception {
        final String fileName = "function_return_value_success.groovy";
        File file = new File(TEST_RESOURCES_PATH + fileName);
        MockMultipartFile upload = new MockMultipartFile(API_SCRIPTS_UPLOAD_REQUEST_PARAMETER_MULTIPART_FILE, fileName,
                MediaType.APPLICATION_OCTET_STREAM_VALUE,
                Files.readAllBytes(file.toPath()));
        mockMvc.perform(multipart(API_SCRIPTS_UPLOAD_URL).file(upload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_ID, notNullValue()))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_STATUS, is("UPLOADED")))
                .andExpect(jsonPath("$." + API_SCRIPTS_EXECUTE_RESPONSE_EXECUTION_RESULT, is("null")));
    }

    // Below are test cases that get the script execution result the execution ID
    @Test
    public void testGetGroovyScriptExecutionHistoryNotFoundWhenExecutionIdDoesNotExist() throws Exception {
        mockMvc.perform(get(API_SCRIPTS_EXECUTION_URL + "execution_id_does_not_exist"))
                .andExpect(status().isNotFound());
    }

    // Below are test cases that execute the uploaded script by the execution ID
    @Test
    public void testExecuteGroovyScriptExecutionHistoryNotFoundWhenExecutionIdDoesNotExist() throws Exception {
        mockMvc.perform(post(API_SCRIPTS_EXECUTION_URL + "execution_id_does_not_exist"))
                .andExpect(status().isNotFound());
    }
}