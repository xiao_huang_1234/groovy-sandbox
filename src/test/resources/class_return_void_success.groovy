package com.behavox.script.sandbox.resources

class Employee {
    private int id;
    private String name;

    int getId() {
        return this.id;
    }

    void setId(int id) {
        this.id = id;
    }

    String getName() {
        return this.name;
    }

    void setName(String name) {
        this.name = name;
    }

    static void main(String[] args) {
        Employee employee = new Employee();
        employee.setId(100);
        employee.setName("Rocky");

        println(employee.getId());
        println(employee.getName());
    }
}

