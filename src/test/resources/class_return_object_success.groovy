package com.behavox.script.sandbox.resources

class Member {
    private int id;
    private String name;

    int getId() {
        return this.id;
    }

    void setId(int id) {
        this.id = id;
    }

    String getName() {
        return this.name;
    }

    void setName(String name) {
        this.name = name;
    }

    String toString() {
        return "{id:" + this.id + ", name:'" + this.name + "'}"
    }
}

def createMember(id, name) {
    Member member = new Member();
    member.setId(id);
    member.setName(name);
    return member;
}

createMember(200, "Marshall");

