## Groovy Sandbox

A web-service accepts Groovy code, executes it and handles the result.

## Features implemented

Below are the major features implemented with this service.

**1. An API endpoint "/v1/scripts/upload_execute" that saves the uploaded Groove script to file system,
 synchronously execute the script, save the execution result to H2 database and return the result;**

**2. A simple frontend UI to upload and execute Groovy scripts, and display the script execution results;**

**3. An API endpoint "/execution/{scriptExecutionId}" to query the script execution result by its unique execution ID;**

**4. An API endpoint "/v1/scripts/upload" that only saves the uploaded Groove script to file system,
 and return an unique execution ID for future query and execution;**

**5. An API endpoint "/execution/{scriptExecutionId}" that executes the uploaded Groove script by its unique execution ID,
 and return the execution result;**

**6. The suit of API endpoints #3, #4, and #5 can be used to save the uploaded Groove scripts to file system,
 then asynchronously execute Groovy scripts, and query the execution status during the process.**

## Technical debts

The most important technical debt is missing `Groovy source code analyzer` to validate scripts against the defined security rules and other business logics.

Below are some other technical debts present within current implementation:

**1. File system on the application server may run out of free spaces due to large volumes of script file uploads;**

**2. Scripts to upload will overwrite the previously uploaded script with the same original file name;**

**3. The whole execution histories stored within in-memory database H2 will be lost after the application server is stopped;**

**4. Script execution results are kept as a limited number (200 by default) of characters;**

**5. The message broker and background script execution workers are not implemented for asynchronous script executions;**

**6. Security policies are not in place;**

**7. Some edge cases of integration test are not covered;**

**8. Unit tests are not implemented;**


## Potential future works

The most important new feature is to integrate and/or implement a Groovy source code analyzer and/or a rule engine 
with background script execution workers along with other surrounding systems to perform some tasks, including but not limited to:

**1. check styles;**

**2. validate methods and language features to prevent malicious code;**

**3. stop the scipt execution if defined security rules are violated;**

**4. notify our system administrators to intervene potential dangerous script executions;**

**5. system administrators could review and approve potential dangerous script executions;**


Below are some other potential future works to make this service production ready:

**1. Replace file system with S3 as uploaded script storage for better availability and scalability;**

**2. Generate unique upload script file URLs (by prepending target file name with UUID or timestamp?)
 to prevent scripts with the same original file name from overwriting each other;**
 
**3. Replace in-memory database H2 with a persistent data store to store the script execution history;**
 
**4. Choose a NoSQL database to store the full information of the script execution result as JSON object;**

**5. Implement message broker and background script execution workers with Saga pattern to scale out the system;**

**6. Implement security policies for authentication, authorization and accounting;**

**7. Implement some edge cases of integration tests;**

**8. Implement unit tests;**

**9. Apply Event Sourcing pattern to keep track of script executions if auditing is required;**

**10. Implement APIs and frontend UI to support download, display, edits and debug of scripts;**
 

## Steps to run and test the application

Below are steps to run and test the application.

**1. Set up the environment variables**

Open a bash terminal and specify a folder within the local system to store the uploaded Groovy script files.

Below is the sample command.

```bash
export FILE_UPLOADPATH=/Users/xhuang/uploads
```

**2. Run the application**

Package the application as a jar file and run the jar as follows:

```bash
cd groovy-sandbox
mvn clean package
java -jar target/groovy-sandbox-0.0.1-SNAPSHOT.jar
```

**3. Try out the application**

The application frontend UI is available at http://localhost:8080.

You could choose your Groovy script files, or you could use the sample Groovy script files 
that comes with the application under the source folder "src/test/resources" mostly used for integration tests.

After you select a Groovy script and submit it for the execution, 
you could see the script execution result shown on the frontend UI.

You can also run the following curl script to check the script execution result:

```bash
curl -X GET "http://localhost:8080/v1/scripts/execution/b3bd1114-21be-4a11-9693-f4b3f81779ee"
```
Please replace the sample execution ID `b3bd1114-21be-4a11-9693-f4b3f81779ee` with the actual execution ID you got from the frontend UI.

Another way to check the script execution results is through H2 database console, 
which can be accessed at http://localhost:8080/h2 with the default configurations as follows:

```bash
Driver Class: org.h2.Driver
JDBC URL: jdbc:h2:mem:testdb
User Name: sa
Password:
```

After you connect to H2 database, you may run the following SQL query to check the script execution histories.

```bash
SELECT * FROM SCRIPT_EXECUTION_HISTORY
```

